const router = require('express').Router();
const controller = require('./controller');

router.get('/getTags', async (req, res) => {
  try {
    const tags = await controller.getFeedingFoodTags();
    res.status(200).send(tags);
  } catch (err) {
    res.status(500).send(err.message);
  }
});

router.post('/submit', async (req, res) => {
  try {
    await controller.createFeeding(req.body);
    res.status(201).send();
  } catch (err) {
    res.status(500).send(err.message);
  }
});

router.get('/data', async (req, res) => {
  try {
    const data = await controller.getFeedingData();
    res.status(200).send(data);
  } catch (err) {
    res.status(500).send(err.message);
  }
});

module.exports = router;
