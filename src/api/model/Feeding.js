const uuid = require('uuid');

const CouchDB = require('../database/CouchDB');

const Feeding = {
  async getTags() {
    const response = await CouchDB.query('feedings/tags', { reduce: true, group: true });
    const { rows } = response;

    const tags = rows.map(row => row.key[0]);
    return tags;
  },

  async create(data) {
    data._id = uuid.v4(); // eslint-disable-line
    data.docType = 'feeding';
    await CouchDB.put(data);
  },

  async export() {
    const response = await CouchDB.query('feedings/all', { include_docs: true });
    const { rows } = response;

    const feedings = rows.map((row) => {
      const { doc } = row;

      delete doc._id; // eslint-disable-line
      delete doc._rev; // eslint-disable-line
      delete doc.docType;

      return doc;
    });

    return feedings;
  },
};

module.exports = Feeding;
