const PouchDB = require('pouchdb');

const database = 'duck-feed';

const { COUCHDB_URL } = process.env;

const ddoc = {
  _id: '_design/feedings',
  views: {
    all: {
      map: function (doc) {
        if (doc.docType === 'feeding') {
          // @ts-ignore
          emit(doc._id); // eslint-disable-line
        }
      }.toString(),
    },
    tags: {
      map: function (doc) {
        if (doc.docType === 'feeding') {
          for (var i = 0; i < doc.foodTags.length; i++) { // eslint-disable-line
            // @ts-ignore
            emit([doc.foodTags[i]], 1); // eslint-disable-line
          }
        }
      }.toString(),

      reduce: function(keys, values, rereduce) { // eslint-disable-line
        // @ts-ignore
        return sum(values); // eslint-disable-line
      }.toString(),
    },
  },
};

const CouchDB = COUCHDB_URL ? new PouchDB(`${COUCHDB_URL}/${database}`) : new PouchDB(database);

async function initialize() {
  try {
    await CouchDB.put(ddoc);
  } catch (err) {
    if (err.status !== 409) {
      throw err;
    }
  }
}

initialize();

module.exports = CouchDB;
