const Feeding = require('./model/Feeding');

const controller = {
  async getFeedingFoodTags() {
    const tags = await Feeding.getTags();
    return tags;
  },

  async createFeeding(data) {
    await Feeding.create(data);
  },

  async getFeedingData() {
    const data = await Feeding.export();
    return data;
  },
};

module.exports = controller;
