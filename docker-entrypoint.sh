#!/bin/bash

cd /usr/src/app

HOST=0.0.0.0 COUCHDB_URL="${COUCHDB_URL}" npm run start
