# duck-feed

## Overview

`duck-feed` is a web application created to allow researchers to record various metrics about when happy ducks are fed all around the world. It also provides convenient export functionality, which gives any user of the application the ability to export all collected data in JSON format and analyze it with any tool they like.

## Summary

The stretch goal to implement an automatic schedule for duck feeding was not implemented. This was due to the amount of time the author could dedicate to working on this project. At minimum, it is the author's opinion that this feature *requires* a proper account system with login capability and a back-end routine to run CRON-like jobs. Neither of these is particularly challenging, and the author would be happy to share how this could have been implemented when asked.

## Technology

### Breakdown

```
Database: CouchDB
Web Server: Express.js
Front-End: Vue.js
Back-End: Node.js
```

### Justification

`CouchDB`

CouchDB is dead simple NoSQL database where every value is a JSON document. It is well suited to lightweight web applications as there is generally no need for multi-record transaction support. It generally shines under this use case as it scales reads and writes horizontally due to leveraging master-master database replication. It favours Availability and Partitioning over Consistency to achieve this high scalability. It was chosen for these reasons, as well as because *the author is quite familiar with it*.

`Express.js`

Express is a premier web server for Node.js and is often the only letter in the well-known `MEAN` stack that sticks. If this isn't enough of a reason to choose it, it provides a dead simple middleware pattern that makes combining routes a breeze. It was chosen for this reason, as well as because *the author is quite familiar with it*.

`Vue.js`

Vue is a reactive framework that borrows concepts from React as well as early Angular versions. It was created by Evan You - an ex-Googler. It is known as a bit of sweet spot between the two, as it's not quite as heavy, opinionated and "batteries included" as Angular, nor is it as barebones as React. It is modular, which allows you to swap the first party libraries for any of your choosing. *This design philosophy resonates with the author for multiple reasons and that's why it was chosen.*

`Nuxt.js` **is a framework for Vue that allows server or client side rendering. It pre-configures more for you and was modelled after the React project** `Next.js`

`Node.js`

JavaScript all the way down! Why not? Node.js excels at asynchronous programming and turns out to be no slouch when it comes to web servers as a result. It certainly helps that the V8 Chrome engine has great optimization too. JavaScript can allow incredible development speed for both new and experienced programmers as long as a little discipline is applied. If the target is the web, Node.js will handle what's needed. *It is for these reasons it was selected, plus the author happens to like it already*.

## Architecture

### Application

This application could have been created more forward-thinking with a microservice architecture, but due to time availability of the author it was decided to stick to a more conventional architecture:

![arch.png](docs/images/arch.png)

As you can see above, the application is comprised of a single node serving both the front-end client code and the back-end requests originating from the client. The server interacts with a single database instance for storing and retrieving feeding data.

### Data Model

As a NoSQL database was used, the data model consists of a single-document schema. This is wholly acceptable for the defined task, but there are certainly cases where one would want to split document data in CouchDB.

```json
{
  "_id": "(string) <uuidv4>",
  "_rev": "(string) <The document revision>",
  "docType": "feeding",
  "timestamp": "(string) <ISO format>",
  "food": "(string) <The food>",
  "location": "(string) <The feeding location>",
  "numDucks": "(number) <The number of ducks fed>",
  "foodTags": [ "(string) <The type of food>" ],
  "foodAmount": "(number) <The amount of food in kilograms>"
}
```

## Code Structure

The code is largely organized in a way that is enforced or recommended by the Nuxt framework. The server code fits into this model by being created as Express middleware in `src/api`. This convention appears to be because Vue is a front-end framework and normally works on the assumption your back-end is a set of REST APIs. In this case, that meaning is overloaded to provide a "local REST API", or back-end.

The back-end is split into four modest layers: router, controller, model, database. This is a rather common layout used to better separate logic and make it more flexible when it comes to swapping pieces out. The router handles incoming calls to the backend and forwards to the controller function suited to handle that request. The controller interacts with the model, which is a data object abstraction layer that sits on top of the database.

Due to the available time of the author on this project, the controller is a little on the boring side. Normally it would be packed with more logic to perform strict server-side validation of input received from the front-end for **security** reasons, but that logic is absent in this case.

## Deployment

The application is built as a `Docker` container and is deployed to a `Kubernetes` cluster operated by the author. The database is also containerized and running in this same environment.

## Time Spent

Time spent on this was roughly **a work day**. A lot of that was spent fighting `webpack` behaviour at various moments, which is a rage story any front-end developer is familiar with.

## Improvements

### General

- Split back-end functionality into an external REST API so it can be called by mobile apps as well as the main web application
- Add external job for processing and adding scheduled feedings
- Decide whether server-side rendering is valuable to this application, or instead generate a bundle that can be hosted by any web server or Amazon S3
- Better handling of potential errors in both the front and back end
- Pop notification on error in the front-end
- Add multi-proccessor cluster support to the Node.js back-end **depending on deployment vector** - containerized applications don't really require this as they can be scaled behind a load balancer seamlessly, effectively achieving the same result


### Security

- Stricter input validation
- Consideration for input sanitization to prevent possibilities of script injection around the application
- Constraints on payload size on the server side to prevent it from being overwhelmed
- Basic authentication login with hashed (and salted) password storage in the database
- JSON web tokens for continued authentication without needing to hit login server
