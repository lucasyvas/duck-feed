module.exports = {
  srcDir: 'src/',
  head: {
    title: 'Duck Feed',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Duck Feed' },
    ],
    link: [
      { rel: 'stylesheet', href: 'https://cdn.materialdesignicons.com/2.4.85/css/materialdesignicons.min.css' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Pacifico' },
    ],
    htmlAttrs: {
      style: 'margin: 0; padding: 0; height: 100%; width: 100%; background-color: #f2f2f2',
    },
    bodyAttrs: {
      style: 'margin: 0; padding: 0; height: 100%; width: 100%; background-color: #f2f2f2',
    },
  },
  css: [
    { src: 'assets/style/scss/buefy.overrides.scss', lang: 'scss' },
  ],
  plugins: [
    'plugins/buefy',
  ],
  build: {
    vendors: [
      'axios',
      'buefy',
    ],
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }

      if (ctx.isClient) {
        config.devtool = 'eval-source-map';
      }

      config.node = {
        fs: 'empty',
        net: 'empty',
      };
    },
  },
  serverMiddleware: [
    '~/api/index.js',
  ],
};
