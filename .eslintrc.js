module.exports = {
  root: true,
  parser: 'babel-eslint',
  extends: 'airbnb-base',
  env: {
    browser: true,
    node: true,
  },
  rules: {
    'no-plusplus': 0,
    'import/no-extraneous-dependencies': 0,
    'no-shadow': 0,
    'no-param-reassign': 0,
    'import/prefer-default-export': 0,
    'no-underscore-dangle': [1, { allowAfterThis: true }],
    'func-names': 0,
    'space-before-function-paren': 0,
    radix: 0,
  },
  plugins: [
    'html',
  ],
};
